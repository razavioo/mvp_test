package com.andraulic.mvptest.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.andraulic.mvptest.R;
import com.andraulic.mvptest.activity.add.fragment.ListPersonsPresenter;
import com.model.Person;

import io.realm.RealmResults;

/**
 * Created by razavioo on 22/07/2018.
 */
public class ListPersonsAdapter extends RecyclerView.Adapter {
    private RealmResults<Person> personList;
    private ListPersonsPresenter presenter;

    public ListPersonsAdapter(RealmResults<Person> personList, ListPersonsPresenter presenter) {
        this.personList = personList;
        this.presenter = presenter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VHListPersons(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.person_item, parent, false), presenter);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((VHListPersons) holder).bind(personList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }
}