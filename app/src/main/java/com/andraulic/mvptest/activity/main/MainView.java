package com.andraulic.mvptest.activity.main;

import com.base.View;

/**
 * Created by razavioo on 22/07/2018.
 */
public interface MainView extends View {
    void onShowFragment();
}
