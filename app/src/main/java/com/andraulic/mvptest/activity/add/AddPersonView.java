package com.andraulic.mvptest.activity.add;

import com.base.View;

/**
 * Created by razavioo on 22/07/2018.
 */
public interface AddPersonView extends View {
    void onPersonSaved();
}
