package com.andraulic.mvptest.activity.add.fragment;

import com.base.Presenter;
import com.base.ViewHolder;
import com.base.ViewHolderPresenter;
import com.model.Person;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by razavioo on 22/07/2018.
 */
public class ListPersonsPresenter
        implements Presenter<ListPersonsView>, ViewHolderPresenter<ViewHolder> {

    private ListPersonsView listPersonsView;
    private ViewHolder viewHolder;
    private RealmResults<Person> persons;
    private Realm realm;

    @Override
    public void onAttach(ListPersonsView View) {
        this.listPersonsView = View;
    }

    @Override
    public void onAttach(ViewHolder ViewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void onDetach() {
        this.listPersonsView = null;
        this.viewHolder = null;
    }

    public void showResult() {
        realm = Realm.getDefaultInstance();
        persons = realm.where(Person.class).findAll();
        listPersonsView.onShowResult(persons);
    }

    public void deleteData(final int pos) {
        realm = Realm.getDefaultInstance();
        persons = realm.where(Person.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realmDel) {
                persons.get(pos).deleteFromRealm();
            }
        });

        showResult();
    }
}
