package com.andraulic.mvptest.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andraulic.mvptest.R;
import com.andraulic.mvptest.activity.add.fragment.ListPersonsPresenter;
import com.base.ViewHolder;
import com.model.Person;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by razavioo on 22/07/2018.
 */

public class VHListPersons extends RecyclerView.ViewHolder implements ViewHolder {

    @BindView(R.id.person_item_name)
    TextView txtName;
    @BindView(R.id.person_item_family_name)
    TextView txtFamilyName;
    @BindView(R.id.person_item_delete)
    TextView txtDelete;

    private ListPersonsPresenter presenter;

    public VHListPersons(View itemView, ListPersonsPresenter presenter) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.presenter = presenter;
    }

    @Override
    public void bind(final Person person, final int pos) {
        txtName.setText(person.getName());
        txtFamilyName.setText(person.getFamilyName());

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(txtDelete.getContext(), "حذف شد.", Toast.LENGTH_SHORT).show();
                presenter.deleteData(pos);
            }
        });
    }
}