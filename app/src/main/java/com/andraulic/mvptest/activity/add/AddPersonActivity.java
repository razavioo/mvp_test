package com.andraulic.mvptest.activity.add;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.andraulic.mvptest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by razavioo on 22/07/2018.
 */
public class AddPersonActivity extends AppCompatActivity implements AddPersonView {
    @BindView(R.id.add_person_name_title)
    TextInputEditText txtName;

    @BindView(R.id.add_person_family_name_title)
    TextInputEditText txtFamilyName;

    @BindView(R.id.add_person_save)
    Button btnSave;

    AddPersonPresenter addPersonPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);
        ButterKnife.bind(this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString();
                String familyName = txtFamilyName.getText().toString();

                if (!name.isEmpty() & !familyName.isEmpty()) {
                    addPersonPresenter.savePerson(name, familyName);
                }
            }
        });

        addPersonPresenter = new AddPersonPresenter();

        onAttachView();
    }

    @Override
    public void onPersonSaved() {
        Toast.makeText(this, "فرد مورد نظر ذخیره شد.", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void onAttachView() {
        addPersonPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        addPersonPresenter.onDetach();
    }

    @Override
    protected void onDestroy() {
        onDetachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        onDetachView();
        super.onBackPressed();
    }
}
