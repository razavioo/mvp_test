package com.andraulic.mvptest.activity.main;

import com.base.Presenter;

/**
 * Created by razavioo on 22/07/2018.
 */
public class MainPresenter implements Presenter<MainView> {
    private MainView mainView;

    @Override
    public void onAttach(MainView View) {
        mainView = View;
    }

    @Override
    public void onDetach() {
        mainView = null;
    }

    public void getPersonList() {
        mainView.onShowFragment();
    }
}
