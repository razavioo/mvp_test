package com.andraulic.mvptest.activity.add;

import com.base.Presenter;
import com.model.Person;

import java.util.UUID;

import io.realm.Realm;

/**
 * Created by razavioo on 22/07/2018.
 */
public class AddPersonPresenter implements Presenter<AddPersonView> {
    private AddPersonView addPersonView;

    @Override
    public void onAttach(AddPersonView View) {
        addPersonView = View;
    }

    @Override
    public void onDetach() {
        addPersonView = null;
    }

    public void savePerson(String name, String familyName) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        Person person = realm.createObject(Person.class, generateID());
        person.setName(name);
        person.setFamilyName(familyName);
        realm.commitTransaction();
        realm.close();

        addPersonView.onPersonSaved();
    }

    private String generateID() {
        return UUID.randomUUID().toString();
    }
}
