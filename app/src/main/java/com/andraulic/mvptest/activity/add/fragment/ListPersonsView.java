package com.andraulic.mvptest.activity.add.fragment;

import com.base.View;
import com.model.Person;

import io.realm.RealmResults;

/**
 * Created by razavioo on 22/07/2018.
 */
public interface ListPersonsView extends View {
    void onShowResult(RealmResults<Person> notes);
}
