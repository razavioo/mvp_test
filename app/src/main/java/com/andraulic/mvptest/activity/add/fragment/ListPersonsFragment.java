package com.andraulic.mvptest.activity.add.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andraulic.mvptest.R;
import com.andraulic.mvptest.activity.adapter.ListPersonsAdapter;
import com.model.Person;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;

/**
 * Created by razavioo on 22/07/2018.
 */
public class ListPersonsFragment extends Fragment implements ListPersonsView {

    @BindView(R.id.list_persons_recycler)
    RecyclerView rvPersons;
    ListPersonsPresenter listPersonsPresenter;
    private View rootView;
    private Unbinder unbinder;

    public ListPersonsFragment() {
    }

    public static Fragment newInstance() {
        return new ListPersonsFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listPersonsPresenter = new ListPersonsPresenter();
        onAttachView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list_persons, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDetachView();
        unbinder.unbind();
    }

    @Override
    public void onShowResult(RealmResults<Person> persons) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        ListPersonsAdapter listPersonsAdapter = new ListPersonsAdapter(persons, listPersonsPresenter);
        rvPersons.setLayoutManager(linearLayoutManager);
        rvPersons.setAdapter(listPersonsAdapter);
    }

    @Override
    public void onAttachView() {
        listPersonsPresenter.onAttach(this);
        showList();
    }

    @Override
    public void onDetachView() {
        listPersonsPresenter.onDetach();
    }

    private void showList() {
        listPersonsPresenter.showResult();
    }
}
