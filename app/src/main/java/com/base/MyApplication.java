package com.base;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by razavioo on 22/07/2018.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        Realm.init(this);
        super.onCreate();
    }
}
