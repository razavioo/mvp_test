package com.base;

import com.model.Person;

/**
 * Created by razavioo on 22/07/2018.
 */

public interface ViewHolder {
    void bind(Person person, int pos);
}