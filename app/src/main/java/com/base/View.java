package com.base;

/**
 * Created by razavioo on 22/07/2018.
 */
public interface View {
    void onAttachView();

    void onDetachView();
}
