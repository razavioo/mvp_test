package com.base;

/**
 * Created by razavioo on 22/07/2018.
 */

public interface Presenter<T extends View> {
    void onAttach(T View);

    void onDetach();
}