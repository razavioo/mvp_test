package com.base;

/**
 * Created by razavioo on 22/07/2018.
 */
public interface ViewHolderPresenter<T extends ViewHolder> {
    void onAttach(T ViewHolder);

    void onDetach();
}