package com.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by razavioo on 22/07/2018.
 */
public class Person extends RealmObject {
    @PrimaryKey
    private String id;

    private String name, familyName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public String toString() {
        return name + " " + familyName;
    }
}
